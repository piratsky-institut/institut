
# Jednací řád Pirátského Institutu

## Čl. 1 Jednání

1. Cílem Jednání je rozhodovat v působnosti orgánu
* a) usnesením které vybírá z předložených návrhů usnesení, nebo
* b) volbou, ve které vybírá z navržených kandidátů

2. Členové orgánu mají právo za podmínek předpisu podat návrhy, účastnit se rozpravy a hlasovat. Svá práva na jednání vykonává každý přímo a osobně.

3. Námitka ???TODO

## Čl. 2 Základní postup

1. Jednání zahájí pověřený člen jen na žádost, pokud mu přijatelný návrh usnesení předloží XXX. Nebo je třeba zvolit členy orgánu, aby byl řádně obsazen, 

2. Rozprava ... (+doba na rozmyšlenou?) TODO

3. V rozhodující hlasování vybere orgán jednu z následujících možností:
* a) schválení některého návrhu usnesení, který pověřený člen zaznamenal,
* b) ukončení projednávání návrhů usnesení bez přijetí usnesení,
* c) vybere z navržených kandidátů takový počet kandidátů, který může být nejvýše zvolen.
