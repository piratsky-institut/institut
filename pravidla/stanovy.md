>Dořešit zda je potřeba a jak:
> 
> + Jednací řád (+zasedání +volby (projít všechny mechanismy))
> + Pravidla hospodaření + Rozpočtová pravidla?
> + Rozhodčí řád?
> + případnou náhradu za Jádro/Kruh (názvy)
> + zastupování strany/orgánu 3. osobou (třeba advokátem) + plná moc

# Stanovy Pirátského Institutu

## Čl 1. Základní ustanovení

1. Název spolku je Pirátský institut z.s. (dále tež "PI")

2. Sídlem spolku je Praha.

## Čl 2. Poslání

1. Institut staví na myšlenkách pirátství a svou činností je dále rozvíjí.

2. Institut chce fungovat jako nezávislá platforma podporující tvořivou spolupráci sdílením bezpečného a spravedlivého prostředí, zdrojů a informací.

3. Institut chce svou činností podporovat a prosazovat zejména:
* a) ochranu soukromí jednotlivce,
* b) transparentní veřejný sektor,
* c) volný tok a sdílení nástrojů, ideálů, kultury, informací a postojů,
* d) nenásílí a humanismus,
* e) různorodost,
* f) stabilitu,
* g) občanskou ekonomiku,
* h) kvalitní legislativu.

## Čl 3. Činnost

1. K dosažení svého poslání Pirátský Institut zejména podporuje a sám uskutečňuje výzkumnou a kulturní činnost v oblastech:
* a) rozvoje demokracie, právního státu, pluralismu a ochrany základních lidských práv,
* b) rozvoje občanské společnosti a společenské soudržnosti,
* c) podpory aktivní účasti občanů na veřejném životě,
* d) zlepšení kvality politické kultury a veřejné diskuse,
* e) přispívání k mezinárodnímu porozumění a spolupráci.
* f) ochrany přírody.

## Čl 4. Členství

1. Řádným členem spolku může být fyzická osoba starší 18 let či právnická osoba, která se ztotožňuje s účelem spolku a chce se aktivně podílet na jeho činnosti. Členství ve spolku vzniká na základě schválení přihlášky Jádrem spolku.

2. Každý řádně přijatý člen se stává členem nejvyššího orgánu spolku.

3. Řádný člen spolku má právo:
* a) účastnit se veškeré činnosti PI, volit a být volen do orgánů spolku.
* b) předkládat návrhy, podněty a připomínky orgánům spolku.
* c) být informován o činnosti spolku.
* d) na veškeré informace o své osobě, vedené v souvislosti s jeho členství,
* e) účastnit se jednání jakéhokoliv orgánu PI, projednává-li tento závažné okolnosti týkající se jeho osoby.

4. Řádný člen spolku má povinnost:
* a) dodržovat znění stanov a řídit se usneseními a rozhodnutími orgánů PI
* b) podílet se na činnosti spolku.
* c) platit pravidelné členské příspěvky, jejichž výši, splatnost a způsob platby určuje Kruh spolku na základě
návrhu Jádra
* d) Dodržovat interní předpisy PI, jsou-li vydány.

5. Členství ve spolku zaniká:
* a) Doručením písemného oznámení o vystoupení člena Jádru spolku.
* b) Nezaplacením členského příspěvku za daný kalendářní rok ani do třetího měsíce ode dne splatnosti
* c) Vyloučením člena z důvodu neplnění jeho povinností vyplývajících z členství v PI nebo z důvodů pravomocného odsouzení za úmyslný trestný čin. Člen je oprávněn do patnácti dnů od doručení rozhodnutí v písemné formě navrhnout, aby rozhodnutí o jeho vyloučení přezkoumala revizní komise. Přezkoumání má odkladný účinek.
* d) Úmrtím člena.

## Čl 5. Orgány

1. Základní orgány PI jsou:
* a) Kruh PI (dále jen "Kruh"), 
* b) Jádro PI (dále jen "Jádro"), 

3. Volby do volených orgánů PI se uskutečňují veřejným hlasováním, pokud není nadpoloviční většinou hlasujících určeno jinak.

4. Členství ve volených orgánech PI zaniká:
* a) rezignací člena,
* b) uplynutím funkčního období,
* c) odvoláním,
* d) zánikem členství v PI.

5. Volené orgány jsou usnášeníschopné při účasti nadpoloviční většiny všech jejich členů. Usnášejí se většinou přítomných členů, není-li stanoveno jinak.

6. Volené orgány PI, jejichž počet členů neklesl pod polovinu členů zvolených Kruhem, mohou kooptovat náhradní členy za členy, jejichž členství v orgánu zaniklo. Funkční období kooptovaných členů je pouze do konce funkčního období daného voleného orgánu.

7. Z každého jednání voleného orgánu PI se bez zbytečného odkladu pořizuje zápis.

8. Každý orgán může zvolit a odvolat pověřeného člena, který se zodpovídá danému orgánu a může plnit předem dané úkoly v rámci pravomocí orgánu jako:
* a) svolávat a řídit jednání orgánu,
* b) jednat jménem orgánu navenek,
* c) a další specificky vyjmenované úkoly.

9. Základní orgány nelze zrušit.

## Čl. 6 Kruh

1. Kruh je nejvyšším orgánem PI a je tvořen shromážděním všech členů PI.

2. Kruhu přísluší:
* a) rozhodovat třípětinovou kvalifikovanou většinou přítomných členů o změnách stanov PI,
* b) schvalovat výroční zprávy o hospodaření PI,
* c) schvalovat jednací řád Kruhu,`
* d) schvaluje pravidla hospodaření PI,` --- ( smazat - všechno potřebné ve stanovách?)`
* e) rozhodovat třípětinovou většinou přítomných členů o zrušení a přeměně PI,
* f) rozhodovat v případě likvidace o jmenování likvidátora,
* g) rozhodovat o odvolání člena proti rozhodnutí Jádra o jeho vyloučení,
* h) rozhodovat o výši, splatnosti a způsobu platby členských příspěvků na základě návrhu Jádra,
* i) určuje počet členů Jádra na další funkční období, volí a odvolává členy Jádra.
* j) zřizuje a ruší orgány PI, které nejsou základní a stanovuje účel a pravidla jejich činnosti.
* k) rozhoduje o navazování spolupráce s tuzemskými i zahraničními organizacemi,

3. Kruh si může vyhradit právo rozhodnout o jakékoli záležitosti, která není stanovami svěřena do působnosti jiného orgánu.

4. Jádro svolává jednání Kruhu podle potřeby, nejméně jednou ročně.

5. Jádro svolá jednání Kruhu vždy, když o to požádá nejméně jedna desetina všech členů Kruhu a to nejpozději do tří měsíců od doručení písemné žádosti.

6. Jednáni Kruhu musí být oznámeno všem členům alespoň 14 dnů přede dnem jeho konáním. Pozvánka musí obsahovat alespoň místo, čas a program jednání. Pozvánka musí být zaslána elektronicky na e-mailovou adresu poskytnutou členem a nelze-li pozvánku zaslat elektronicky nebo uzná-li to svolavatel za vhodné, může být zaslána rovněž poštou.

7. Jednání Kruhu se mají právo zúčastnit všichni členové PI. Každý člen má jeden hlas.

8. Kruh svolaný v souladu s odst. 6 je usnášeníschopný bez ohledu na počet zúčastněných členů PI.

9. Kruh rozhoduje hlasováním. Nenli-li stanoveno jinak, rozhodnutí je přijato, jestliže pro něj hlasuje prostá většina přítomných členů.

10. Záležitost, která nebyla zařazena na pořad jednání Kruhu v zaslané pozvánce, lze zařadit na program jednání jen se souhlasem 2/3 přítomných členů Kruhu.

11. O jednání Kruhu vyhotoví Jádro či pověřený člen Kruhu zápis nejpozději do 14 dnů od jejího konání. Každý člen Kruhu může nahlížet do všech zápisů z jednání Kruhu.

## Čl. 7 Jádro

1. Jádro je statutárním a výkonným orgánem PI, který za svou činnost odpovídá Kruhu.

2. Jádro tvoří 5-9 členů. Počet členů Jádra musí být vždy lichý.

3. Funkční období je 2 roky.

4. Jednání Jádra svolává kterýkoli člen Jádra dle potřeby, nejméně však jednou za 3 měsíce.

5. Na žádost jedné třetiny členů Jádra svolá pověřený člen Jádra jednání Jádra nejpozději do 2 týdnů od doručení žádosti.

6. Jádro zejména:
* a) řídí činnost PI,
* b) zpracovává podklady pro rozhodnutí Kruhu,
* c) předkládá Kruhu výroční zprávy o činnosti a hospodaření,
* d) rozhoduje o přijetí za člena PI,
* e) zřizuje a ruší pracovní skupiny PI, stanovuje účel a pravidla jejich činnosti,
* f) schvaluje interní předpisy PI,
* g) vede seznam členů PI.

7. Jednání Jádra musí být oznámeno elektronicky na e-mailovou adresu uvedenou v seznamu členů všem členům Jádra alespoň 7 dnů před jeho konáním. Z pozvánky musí být zřejmé místo, čas a program jednání. Ve výjimečných případech je možnost i mimořádného jednání, které se může konat s menším časovým odstupem od oznámení. 

## Čl. 8 Hospodaření

1. Za hospodaření PI zodpovídá statutární orgán, který každoročně překládá Kruhu zprávu o hospodaření,

2. O svém hospodaření vede PI účetnictví podle platných právních předpisů a interních předpisů PI.

3. Prostředky PI jsou děleny na volné a vázané. Volné prostředky jsou rovným dílem děleny na části podle aktuálního počtu členů. Každý člen má možnost pomocí hlasování přidělit prostředky ve výši jednoho dílu příslušnému projektu dle čl. 10 stanov. 

4. Přidělené prostředky projektu se stávají vázanými.

5. Účelově určené příjmy konkrétnímu projektu se automaticky přidělí danému projektu při splnění čl.10 7).

6. Nevyužité prostředky v projektu (například jeho ukončením, či zrušením) se vrací zpět do volných prostředků.

7. Všechny příjmy a výdaje jsou soustředěny v jediném systému, který je veřejným informačním systémem spolku vedený jedním či více pověřenými členy.

8. Ve veřejném informačním systému spolku se dále zveřejňují:
* a) faktury, objednávky a doklady na výdaje,
* b) výroční finanční zprávy,
* c) účetní výkazy podle zákona o účetnictví,
* d) projekty a jejich plnění včetně žádostí o proplacení,
* e) veškeré písemné smlouvy,
* f) soupis věcí, které spravuje každý ptojekt, s jejich hodnotou a místem výskytu,
* g) seznam transparentních bankovních účtů,
* h) seznam jiných finančních účtů s přehledem transakcí na nich.

8. Správce projektu je povinen každý výdaj řádně doložit ve stanovené lhůtě dokladem podle zákona. Institut není zavázán uhradit náklady schváleného výdaje, pokud doklad neobdrží ani v dodatečně určené lhůtě.

9. Institut hospodaří elektronicky, není-li stanoveno jinak. Jako bankovní účty institut používá pouze transparentní bankovní účty ve správě finančního odboru. Stavy a transakce na bankovních účtech institutu zveřejňuje banka na internetové stránce.

10. Jádro PI pověří člena správou bankovního účti. Správcovský přístup k účtu PI má pouze tento pověřený člen.

11. Osoba, která má získat jakákoliv přístupová oprávnění k bankovnímu účtu PI, musí před zapsáním tohoto oprávnění u banky podepsat dohodu o hmotné odpovědnosti za neoprávněné převody provedené pod svými uživatelskými údaji a musí být poučena o bezpečnostních pravidlech. Za prověření důvěryhodnosti a spolehlivosti odpovídá Jádro.

12. Pověřený člen převádí peníze z bankovního účtu na žádost o proplacení podanou správcem projektu.

13. Žádost musí obsahovat informaci o druhu výdaje podle třídění stanoveného pověřenou osobou zodpovýdající za převody peněz.

14. Před rozhodnutím o žádosti zkoumá pověřená osoboa zodpovýdající za převody peněz, zda výdaj odpovídá pravidlům hospodaření, zejména
a) zda byl výdaj schválen správcem projektu, který odpovídá za jeho výdaje,
b) zda je výdaj součástí záměru, který je odsouhlasen u projektu, případně zda je připojen souhlas dle podmínek projektu,
c) zda byla projekt schválen a má vázných dostatek peněz pro proplacení,
d) zda byl výdaj hodnověrně doložen, pokud je proplácen zpětně,
e) zda je na příslušném bankovním účtu dostatek prostředků pro proplacení výdaje.

15.  Pověřený člen zamítne žádost, která je v rozporu s pravidly hospodaření. Jinak pověřený člen žádosti vyhoví a žádost proplatí zpětně nebo dopředu na účet příjemce bez zbytečného odkladu.

## Čl 9. Jednání jménem spolku

Právní úkony jménem spolku činní pověřený člen 

## Čl 10. Projekt

1. Projekt je soubor věcných, časových, osobních a finančních podmínek pro činnost potřebnou k dosažení cíle který Kruh v hlasování schválí.

2. Každý projekt má určeného správce. Správce se určí ještě před hlasováním a lze ho měnit pouze novým hlasováním.

3. Projekt může být zrušen rozhodnutím Kruhu.

4. Popis každého projektu musí před svým schválením obsahovat:
* a) název projektu,
* b) krátký srozumitelný popis projektu,
* c) označení správce,
* d) věcné podmínky projektu (vybavení k řešení projektu, které bude nutné zakoupit nebo se souhlasem jeho správce používat),
* e) časové podmínky projektu (termín zahájení a řádného ukončení projektu, členění projektu na úseky, odhadovaný celkový čas strávený prací na projektu),
* f) osobní podmínky projektu (označení osob, které přislíbily věnovat svůj čas projektu)
* g) finanční podmínky projektu (minimální a maximální výše prostředků na projekt či úseky, při jejichž získání bude projekt řešen, orientační rozpočet projektu, další zdroje financování, výše odměny pro řešitele nebo jiné osoby, pokud je navrhována),
* h) upřesnění způsobu a termínu, kdy bude výsledek projektu zveřejněn.
  
5. Popis projektu může definovat velikost částky přpadající na jeden hlas, který v hlasování určuje výši poskytnutých prostředků.

6. Výše prostředků, kterou určitý projekt získal v hlasování, se určí jako násobek počtu hlasů pro něj odevzdaných a částky připadající na jeden hlas.

7. Výše prostředků přidělená projektu nesmí přesáhnout jejich maximální výši, pokud je projektem určena. Pokud je maximální výše prostředků překročena, vrací se přesahující částka zpět do volných prostředků PI.

## Čl 11. Obecné záasady

1. Hlasování probíhá pokud možno elektronicky.

## Čl. 12 Přechodná ustanovení

1. Prvními členy PI se stávají zakladatelé PI, kteří se shodli na stanovách PI.

2. Prvními členy Jádra, jakožto statutárního orgánu jsou JMÉNA ČLENŮ.

3. Funkční období prvních členů Jádra je pouze půl roku.

## Čl. 13 Závěrečná ustanovení

1. V rámci svých pravomocí daných těmito stanovami mohou jednotlivé orgány PI vydávat závazné interní předpisy.

2. Tyto stanovy nabývají platnosti a účinnosti dnem jejich schválení.